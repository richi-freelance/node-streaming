var loggedMiddleware  =  require('../middlewares/rutasprotegidas');


module.exports = exports = function(app, server, socketCallback) {

    app.get('/servidor-senalizacion-compartir', loggedMiddleware, function(req, res){ 

        res.render('compartir-escritorio.ejs');
    });



    console.log("==============================\n module.exports = exports = function(server, socketCallback) { \n =================================");

    /** almacena todos los sockets, user-ids, extra-data y sockets conectados
    puede comprobar la presencia como sigue:
        var isRoomExist = listOfUsers ['room-id']! = Null;
    */
    var listOfUsers = {};

    var cambiosControlesDeModeración = {};

    // for scalable-broadcast demos
    var ScalableBroadcast;

    var io = require('socket.io');

    try {
        // use latest socket.io
        io = io(server);
        io.on('connection', onConnection);
    } catch (e) {
        // otherwise fallback
        io = io.listen(server, {
            log: false,
            origins: '*:*'
        });

        io.set('transports', [
            'websocket',
            'xhr-polling',
            'jsonp-polling'
        ]);

        io.sockets.on('connection', onConnection);
    }

    // to secure your socket.io usage: (via: docs/tips-tricks.md)
    // io.set('origins', 'https://domain.com');

    function serverendUser(socket) {
        console.log("==========      function serverendUser(socket)  ");


        var alreadyExist = listOfUsers[socket.userid];
        var extra = {};

        if (alreadyExist && alreadyExist.extra) {
            extra = alreadyExist.extra;
        }

        /** objeto:   socket.handshake.query

            userid: 'd9c6yl1tpx9',
            sessionid: 'd9c6yl1tpx9',
            msgEvent: 'screen-sharing-demo',
            socketCustomEvent: 'RTCMultiConnection-Custom-Message',
            autoCloseEntireSession: 'false',
            maxParticipantsAllowed: '1000',
            EIO: '3',
            transport: 'polling',
            t: 'LplxjqO' }

        */
        var params = socket.handshake.query;

        console.log("params.extra");
        console.log(params.extra);

        if (params.extra) {
            try {
                if (typeof params.extra === 'string') {
                    params.extra = JSON.parse(params.extra);
                }
                extra = params.extra;
            } catch (e) {
                extra = params.extra;
            }
        }

        listOfUsers[socket.userid] = {
            socket: socket,
            connectedWith: {},
            isPublic: false, // means: isPublicModerator
            extra: extra || {},
            maxParticipantsAllowed: params.maxParticipantsAllowed || 1000
        };


        console.log("listOfUsers");
        console.log(listOfUsers);

    }

    function onConnection(socket) {
        console.log("====================     function onConnection(socket) { ");

        var params = socket.handshake.query;
        var socketMessageEvent = params.msgEvent || 'RTCMultiConnection-Message';

        var sessionid = params.sessionid;
        var autoCloseEntireSession = params.autoCloseEntireSession;

        if (params.enableScalableBroadcast) {
            if (!ScalableBroadcast) {
                ScalableBroadcast = require('./Scalable-Broadcast.js');
            }
            ScalableBroadcast(socket, params.maxRelayLimitPerUser);
        }

        // temporarily disabled
        if (!!listOfUsers[params.userid]) {
            params.dontUpdateUserId = true;

            var useridAlreadyTaken = params.userid;
            params.userid = (Math.random() * 1000).toString().replace('.', '');

            console.log(" EMITIMOS  socket.emit('userid-already-taken', useridAlreadyTaken, params.userid)");
            socket.emit('userid-already-taken', useridAlreadyTaken, params.userid);
        }

        socket.userid = params.userid;
        serverendUser(socket);

        if (autoCloseEntireSession == 'false' && sessionid == socket.userid) {
            socket.shiftModerationControlBeforeLeaving = true;
        }

        socket.on('shift-moderator-control-on-disconnect', function() {
            console.log("---------   socket.on('shift-moderator-control-on-disconnect', function()  ");

            socket.shiftModerationControlBeforeLeaving = true;
        });

        /**  Entra primera para   */
        socket.on('extra-data-updated', function(extra) {
            console.log("---------    socket.on('extra-data-updated', function(extra)  ");

            try {
                if (!listOfUsers[socket.userid]) return;

                listOfUsers[socket.userid].extra = extra;

                console.log("listOfUsers");
                console.log(listOfUsers);

                /**     Objeto    listOfUsers:

                { mi-room-compartido:
                    { 
                        socket:
                          Socket {
                            nsp: [Object],
                            server: [Object],
                            adapter: [Object],
                            id: 'usACDeIa4YsLF8UqAAAB',
                            client: [Object],
                            conn: [Object],
                            rooms: [Object],
                            acks: {},
                            connected: true,
                            disconnected: false,
                            handshake: [Object],
                            fns: [],
                            flags: {},
                            _rooms: [],
                            userid: 'mi-room-compartido',
                            shiftModerationControlBeforeLeaving: true,
                            _events: [Object],
                            _eventsCount: 15 },
                         connectedWith: {},
                         isPublic: false,
                         extra: {},
                         maxParticipantsAllowed: '1000' 
                    } 
                }
                */

                //console.log(JSON.stringify(listOfUsers, null, 4));

                for (var user in listOfUsers[socket.userid].connectedWith){
                    console.log(" EMITIMOS   listOfUsers[user].socket.emit('extra-data-updated', socket.userid, extra), user:", user);

                    listOfUsers[user].socket.emit('extra-data-updated', socket.userid, extra);                    
                }

                
            } catch (e) {
                pushLogs('extra-data-updated', e);
            }
        });

        socket.on('get-remote-user-extra-data', function(remoteUserId, callback) {
            console.log("---------    socket.on('get-remote-user-extra-data', function(remoteUserId, callback)  ");


            callback = callback || function() {};
            if (!remoteUserId || !listOfUsers[remoteUserId]) {
                callback('remoteUserId (' + remoteUserId + ') does NOT exist.');
                return;
            }
            callback(listOfUsers[remoteUserId].extra);
        });

        socket.on('become-a-public-moderator', function() {
            console.log("---------     socket.on('become-a-public-moderator', function()   ");

            try {
                if (!listOfUsers[socket.userid]) return;
                listOfUsers[socket.userid].isPublic = true;
            } catch (e) {
                pushLogs('become-a-public-moderator', e);
            }
        });

        var dontDuplicateListeners = {};
        socket.on('set-custom-socket-event-listener', function(customEvent) {
            if (dontDuplicateListeners[customEvent]) return;
            dontDuplicateListeners[customEvent] = customEvent;

            socket.on(customEvent, function(message) {
                console.log("---------   socket.on(customEvent, function(message) ");

                try {
                    console.log(" EMITIMOS  socket.broadcast.emit(customEvent, message):", customEvent, message);

                    socket.broadcast.emit(customEvent, message);
                } catch (e) {}
            });
        });

        socket.on('dont-make-me-moderator', function() {
            console.log("---------     socket.on('dont-make-me-moderator', function()   ");

            try {
                if (!listOfUsers[socket.userid]) return;
                listOfUsers[socket.userid].isPublic = false;
            } catch (e) {
                pushLogs('dont-make-me-moderator', e);
            }
        });

        socket.on('get-public-moderators', function(userIdStartsWith, callback) {
            console.log("---------  socket.on('get-public-moderators', function(userIdStartsWith, callback)   ");

            try {
                userIdStartsWith = userIdStartsWith || '';
                var allPublicModerators = [];
                for (var moderatorId in listOfUsers) {
                    if (listOfUsers[moderatorId].isPublic && moderatorId.indexOf(userIdStartsWith) === 0 && moderatorId !== socket.userid) {
                        var moderator = listOfUsers[moderatorId];
                        allPublicModerators.push({
                            userid: moderatorId,
                            extra: moderator.extra
                        });
                    }
                }

                callback(allPublicModerators);
            } catch (e) {
                pushLogs('get-public-moderators', e);
            }
        });

        socket.on('changed-uuid', function(newUserId, callback) {
            console.log("---------    socket.on('changed-uuid', function(newUserId, callback)  ");

            callback = callback || function() {};

            if (params.dontUpdateUserId) {
                delete params.dontUpdateUserId;
                return;
            }

            try {
                if (listOfUsers[socket.userid] && listOfUsers[socket.userid].socket.userid == socket.userid) {
                    if (newUserId === socket.userid) return;

                    var oldUserId = socket.userid;
                    listOfUsers[newUserId] = listOfUsers[oldUserId];
                    listOfUsers[newUserId].socket.userid = socket.userid = newUserId;
                    delete listOfUsers[oldUserId];

                    callback();
                    return;
                }

                socket.userid = newUserId;
                serverendUser(socket);

                callback();
            } catch (e) {
                pushLogs('changed-uuid', e);
            }
        });

        socket.on('set-password', function(password) {
            console.log("---------          socket.on('set-password', function(password) ");

            try {
                if (listOfUsers[socket.userid]) {
                    listOfUsers[socket.userid].password = password;
                }
            } catch (e) {
                pushLogs('set-password', e);
            }
        });

        socket.on('disconnect-with', function(remoteUserId, callback) {
            console.log("---------  socket.on('disconnect-with', function(remoteUserId, callback) ");

            try {
                if (listOfUsers[socket.userid] && listOfUsers[socket.userid].connectedWith[remoteUserId]) {
                    delete listOfUsers[socket.userid].connectedWith[remoteUserId];

                    console.log(" EMITIMOS  socket.emit('user-disconnected', remoteUserId)");
                    socket.emit('user-disconnected', remoteUserId);
                }

                if (!listOfUsers[remoteUserId]) return callback();

                if (listOfUsers[remoteUserId].connectedWith[socket.userid]) {
                    delete listOfUsers[remoteUserId].connectedWith[socket.userid];

                    console.log(" EMITIMOS  listOfUsers[remoteUserId].socket.emit('user-disconnected', socket.userid) ");
                    listOfUsers[remoteUserId].socket.emit('user-disconnected', socket.userid);
                }
                callback();
            } catch (e) {
                pushLogs('disconnect-with', e);
            }
        });

        socket.on('close-entire-session', function(callback) {
            console.log("---------    socket.on('close-entire-session', function(callback) ");


            try {
                var connectedWith = listOfUsers[socket.userid].connectedWith;
                Object.keys(connectedWith).forEach(function(key) {
                    if (connectedWith[key] && connectedWith[key].emit) {
                        try {

                            console.log(" EMITIMOS  connectedWith[key].emit('closed-entire-session', socket.userid, listOfUsers[socket.userid].extra) ");
                            connectedWith[key].emit('closed-entire-session', socket.userid, listOfUsers[socket.userid].extra);
                        } catch (e) {}
                    }
                });

                delete cambiosControlesDeModeración[socket.userid];
                callback();
            } catch (e) {
                pushLogs('close-entire-session', e);
            }
        });

        socket.on('check-presence', function(userid, callback) {
            console.log("---------      socket.on('check-presence', function(userid, callback)  ");


            if (!listOfUsers[userid]) {
                callback(false, userid, {});
            } else {
                callback(userid !== socket.userid, userid, listOfUsers[userid].extra);
            }
        });

        function onMessageCallback(message) {
            console.log("---------         function onMessageCallback(message) ");

            try {
                if (!listOfUsers[message.sender]) {

                    console.log(" EMITIMOS  socket.emit('user-not-found', message.sender) ");

                    socket.emit('user-not-found', message.sender);
                    return;
                }

                if (!message.message.userLeft && !listOfUsers[message.sender].connectedWith[message.remoteUserId] && !!listOfUsers[message.remoteUserId]) {
                    listOfUsers[message.sender].connectedWith[message.remoteUserId] = listOfUsers[message.remoteUserId].socket;


                    console.log(" EMITIMOS  listOfUsers[message.sender].socket.emit('user-connected', message.remoteUserId)");
                    listOfUsers[message.sender].socket.emit('user-connected', message.remoteUserId);

                    if (!listOfUsers[message.remoteUserId]) {
                        listOfUsers[message.remoteUserId] = {
                            socket: null,
                            connectedWith: {},
                            isPublic: false,
                            extra: {},
                            maxParticipantsAllowed: params.maxParticipantsAllowed || 1000
                        };
                    }

                    listOfUsers[message.remoteUserId].connectedWith[message.sender] = socket;

                    if (listOfUsers[message.remoteUserId].socket) {
                        console.log(" EMITIMOS  listOfUsers[message.remoteUserId].socket.emit('user-connected', message.sender)");
                        listOfUsers[message.remoteUserId].socket.emit('user-connected', message.sender);
                    }
                }

                if (listOfUsers[message.sender].connectedWith[message.remoteUserId] && listOfUsers[socket.userid]) {
                    message.extra = listOfUsers[socket.userid].extra;

                    console.log(" EMITIMOS listOfUsers[message.sender].connectedWith[message.remoteUserId].emit(socketMessageEvent, message)");
                    listOfUsers[message.sender].connectedWith[message.remoteUserId].emit(socketMessageEvent, message);
                }
            } catch (e) {
                pushLogs('onMessageCallback', e);
            }
        }

        function joinARoom(message) {
            console.log("---------         function joinARoom(message) ");


            var roomInitiator = listOfUsers[message.remoteUserId];

            if (!roomInitiator) {
                return;
            }

            var usersInARoom = roomInitiator.connectedWith;
            var maxParticipantsAllowed = roomInitiator.maxParticipantsAllowed;

            if (Object.keys(usersInARoom).length >= maxParticipantsAllowed) {

                console.log(" EMITIMOS  socket.emit('room-full', message.remoteUserId)");
                socket.emit('room-full', message.remoteUserId);

                if (roomInitiator.connectedWith[socket.userid]) {
                    delete roomInitiator.connectedWith[socket.userid];
                }
                return;
            }

            var inviteTheseUsers = [roomInitiator.socket];
            Object.keys(usersInARoom).forEach(function(key) {
                inviteTheseUsers.push(usersInARoom[key]);
            });

            var keepUnique = [];
            inviteTheseUsers.forEach( function(userSocket) {
                if (userSocket.userid == socket.userid) return;
                if (keepUnique.indexOf(userSocket.userid) != -1) {
                    return;
                }
                keepUnique.push(userSocket.userid);

                message.remoteUserId = userSocket.userid;

                console.log(" EMITIMOS   userSocket.emit(socketMessageEvent, message)");
                userSocket.emit(socketMessageEvent, message);
            });
        }

        var numberOfPasswordTries = 0;

        socket.on(socketMessageEvent, function(message, callback) {
            console.log("---------    socket.on(socketMessageEvent, function(message, callback)  ");
            console.log("---------  socketMessageEvent", socketMessageEvent);


            if (message.remoteUserId && message.remoteUserId === socket.userid) {
                // remoteUserId MUST be unique
                return;
            }

            try {
                if (message.remoteUserId && message.remoteUserId != 'system' && message.message.newParticipationRequest) {
                    if (listOfUsers[message.remoteUserId] && listOfUsers[message.remoteUserId].password) {
                        if (numberOfPasswordTries > 3) {
                            socket.emit('password-max-tries-over', message.remoteUserId);
                            return;
                        }

                        if (!message.password) {
                            numberOfPasswordTries++;
                            socket.emit('join-with-password', message.remoteUserId);
                            return;
                        }

                        if (message.password != listOfUsers[message.remoteUserId].password) {
                            numberOfPasswordTries++;
                            socket.emit('invalid-password', message.remoteUserId, message.password);
                            return;
                        }
                    }

                    if (listOfUsers[message.remoteUserId]) {
                        joinARoom(message);
                        return;
                    }
                }

                if (message.message.shiftedModerationControl) {
                    if (!message.message.firedOnLeave) {
                        onMessageCallback(message);
                        return;
                    }
                    cambiosControlesDeModeración[message.sender] = message;
                    return;
                }

                // for v3 backward compatibility; >v3.3.3 no more uses below block
                if (message.remoteUserId == 'system') {
                    if (message.message.detectPresence) {
                        if (message.message.userid === socket.userid) {
                            callback(false, socket.userid);
                            return;
                        }

                        callback(!!listOfUsers[message.message.userid], message.message.userid);
                        return;
                    }
                }

                if (!listOfUsers[message.sender]) {
                    listOfUsers[message.sender] = {
                        socket: socket,
                        connectedWith: {},
                        isPublic: false,
                        extra: {},
                        maxParticipantsAllowed: params.maxParticipantsAllowed || 1000
                    };
                }

                // if someone tries to join a person who is absent
                if (message.message.newParticipationRequest) {
                    var waitFor = 60 * 10; // 10 minutes
                    var invokedTimes = 0;
                    (function repeater() {
                        if (typeof socket == 'undefined' || !listOfUsers[socket.userid]) {
                            return;
                        }

                        invokedTimes++;
                        if (invokedTimes > waitFor) {
                            socket.emit('user-not-found', message.remoteUserId);
                            return;
                        }

                        if (listOfUsers[message.remoteUserId] && listOfUsers[message.remoteUserId].socket) {
                            joinARoom(message);
                            return;
                        }

                        setTimeout(repeater, 1000);
                    })();

                    return;
                }

                onMessageCallback(message);
            } catch (e) {
                pushLogs('on-socketMessageEvent', e);
            }
        });

        socket.on('disconnect', function() {
            console.log("---------          socket.on('disconnect', function()   ");


            try {
                if (socket && socket.namespace && socket.namespace.sockets) {
                    delete socket.namespace.sockets[this.id];
                }
            } catch (e) {
                pushLogs('disconnect', e);
            }

            try {
                var message = cambiosControlesDeModeración[socket.userid];

                if (message) {
                    delete cambiosControlesDeModeración[message.userid];
                    onMessageCallback(message);
                }
            } catch (e) {
                pushLogs('disconnect', e);
            }

            try {
                // inform all connected users
                if (listOfUsers[socket.userid]) {
                    var firstUserSocket = null;

                    for (var s in listOfUsers[socket.userid].connectedWith) {
                        if (!firstUserSocket) {
                            firstUserSocket = listOfUsers[socket.userid].connectedWith[s];
                        }

                        listOfUsers[socket.userid].connectedWith[s].emit('user-disconnected', socket.userid);

                        if (listOfUsers[s] && listOfUsers[s].connectedWith[socket.userid]) {
                            delete listOfUsers[s].connectedWith[socket.userid];
                            listOfUsers[s].socket.emit('user-disconnected', socket.userid);
                        }
                    }

                    if (socket.shiftModerationControlBeforeLeaving && firstUserSocket) {
                        firstUserSocket.emit('become-next-modrator', sessionid);
                    }
                }
            } catch (e) {
                pushLogs('disconnect', e);
            }

            delete listOfUsers[socket.userid];
        });


        if (socketCallback) {
            socketCallback(socket);
        }
    }
};

var enableLogs = false;

try {
    var _enableLogs = require('./config.json').enableLogs;

    if (_enableLogs) {
        enableLogs = true;
    }
} catch (e) {
    enableLogs = false;
}

var fs = require('fs');

function pushLogs() {
    console.log("==========  function pushLogs()  ");

    if (!enableLogs) return;
    var logsFile = process.cwd() + '/logs.json';
    var utcDateString = (new Date).toUTCString().replace(/ |-|,|:|\./g, '');

    // uncache to fetch recent (up-to-dated)
    uncache(logsFile);

    var logs = {};

    try {
        logs = require(logsFile);
    } catch (e) {}

    if (arguments[1] && arguments[1].stack) 
        arguments[1] = arguments[1].stack;

    try {
        logs[utcDateString] = JSON.stringify(arguments, null, '\t');
        fs.writeFileSync(logsFile, JSON.stringify(logs, null, '\t'));
    } catch (e) {
        logs[utcDateString] = arguments.toString();
    }
}

// Removiendo JSON de cache
function uncache(jsonFile) {
    console.log("==========  function uncache(jsonFile)  ");

    searchCache(jsonFile, function(mod) {
        delete require.cache[mod.id];
    });

    Object.keys(module.constructor._pathCache).forEach(function(cacheKey) {
        if (cacheKey.indexOf(jsonFile) > 0) {
            delete module.constructor._pathCache[cacheKey];
        }
    });
}

function searchCache(jsonFile, callback) {
    console.log("==========  function searchCache(jsonFile, callback)  ");

    var mod = require.resolve(jsonFile);

    if (mod && ((mod = require.cache[mod]) !== undefined)) {
        (function run(mod) {
            mod.children.forEach(function(child) {
                run(child);
            });

            callback(mod);
        })(mod);
    }
}
