// server.js

// ======================================================================

var express  	= 	require('express');
var http 		=   require('http');
var session  	= 	require('express-session');
var cookieParser = 	require('cookie-parser');
var bodyParser  = 	require('body-parser');
var morgan 	 	= 	require('morgan');
var app      	= 	express();
var port     	= 	process.env.PORT || 8080;

var server = http.createServer(app);
var io  = require('socket.io').listen(server);

/**
var server    = app.listen(9001);
var io        = require('socket.io').listen(server);
*/



var passport 	= 	require('passport');
var flash    	= 	require('connect-flash');

// configuration ===============================================================
// connect to our database

require('./config/passport')(passport); // pasamos passport para configuracion

app.use(morgan('dev')); // debug

app.use(cookieParser()); // read cookies (con auth)
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

// nuestro motor de template
app.set('view engine', 'ejs');


// required for passport
app.use(session({
	secret: 'keysupersecret',
	resave: true,
	saveUninitialized: true
 } )); // session secret

app.use(passport.initialize());
app.use(passport.session()); // persistencia sesiones
app.use(flash()); // usamos connect-flash para almacenamiento de sesiones con flash 


/** ==========================================================
	routes 
========================================================== */
require('./controllers/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

require('./controllers/chat-sockets.js')(app, io); // load our routes and pass in our app and fully configured passport




/**   ================================================
	Compartir escritorio
================================================  */

function resolveURL(url) {
    var isWin = !!process.platform.match(/^win/);
    if (!isWin) return url;
    return url.replace(/\//g, '\\');
}


var fs = require('fs');
var path = require('path');

// see how to use a valid certificate:
// https://github.com/muaz-khan/WebRTC-Experiment/issues/62
var options = {
    key: fs.readFileSync(path.join(__dirname, resolveURL('fake-keys/privatekey.pem'))),
    cert: fs.readFileSync(path.join(__dirname, resolveURL('fake-keys/certificate.pem')))
};


require('./controllers/servidor-senalizacion-compartir.js')(app, server, function(socket) {

    try {
        var params = socket.handshake.query;

        /*** "socket" el objeto está totalmente en sus propias manos!

        en su página HTML, puede acceder a socket como sigue:
            connection.socketCustomEvent = 'mensaje personalizado';
            var socket = connection.getSocket ();
            socket.emit (connection.socketCustomEvent, {test: true});
        */
        if (!params.socketCustomEvent) {
            params.socketCustomEvent = 'custom-message';
        }

        socket.on(params.socketCustomEvent, function(message) {
            try {
                socket.broadcast.emit(params.socketCustomEvent, message);
            } catch (e) {}
        });
    } catch (e) {}
});

/**   ================================================
	fin Compartir escritorio
================================================  */


server.listen(port, null, function() {
    console.log("Listening on port " + port);
});
